require 'rails_helper'
# require 'rails-controller-testing'
RSpec.describe ProductsController, type: :controller do
    
    before(:all) do 
        @product1 = Product.create(name:"name11", description:"des11--", price:25.0)
        @product2 = Product.create(name:"name22", description:"des22--", price:29.78)
        @product3 = Product.create(name:"name33", description:"des33--", price:22.85)
        @product4 = Product.create(name:"name44", description:"des44--", price:43.25)
    end 
    
    after(:all) do
        Product.delete_all
    end
  
  # list all products
  # /products
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "Returns all created products" do
      get :index
      expect(assigns(:products)).to match_array([@product1, @product2, @product3, @product4])
    end
  end


end
