# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Product.create(name: 'name1', description: 'des1..', price: 10.1)
Product.create(name: 'name2', description: 'des2..', price: 20.2)
Product.create(name: 'name3', description: 'des3..', price: 30.3)
Product.create(name: 'name4', description: 'des4..', price: 40.4)
Product.create(name: 'name5', description: 'des5..', price: 50.5)
Product.create(name: 'name6', description: 'des6..', price: 60.6)

