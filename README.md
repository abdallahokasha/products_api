## Installation

Firstly, Install Ruby & Rails from [gorails setup guide](https://gorails.com/setup/ubuntu/16.04)

Note: Installation guide for OS & windows also provided in the above guide.


After installing Ruby and Rails using rbenv (in my case) we can use commands to know installed versions

I am using:
 
`ruby -v`
ruby 2.5.3p105 (2018-10-18 revision 65156) [x86_64-linux]

`rails -v` 
Rails 5.2.2

`rbenv -v`
rbenv 1.1.1-39-g59785f6


## create new Rails app

`rails new products_api --api`

Note: we use `--api` options to create api without views.

Finally, we use `rails s` to run our rails app.
